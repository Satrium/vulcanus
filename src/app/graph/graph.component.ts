import { AfterViewInit, Component, ElementRef,  EventEmitter,  HostListener, OnDestroy,  Output,  ViewChild, inject, input, signal } from '@angular/core';
import { toObservable } from '@angular/core/rxjs-interop';
import ForceGraph3D, { ForceGraph3DInstance } from '3d-force-graph';
import { LayoutService } from '../layout/service/app.layout.service';
import { DataStore } from '../data/stores/data.store';
import { Subscription, combineLatest, map, tap, throttleTime } from 'rxjs';

import SpriteText from 'three-spritetext';
import { Vector3 } from 'three';
import { EntityType, GraphEdge, GraphNode, GraphStore } from './service/graph.store';
import { GraphService, weightByType } from './service/graph.service';

import { FilterConfig } from './models/filter.config';

import { AutoCompleteModule } from 'primeng/autocomplete';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-graph',
  standalone: true,
  imports: [AutoCompleteModule, CommonModule, FormsModule],
  templateUrl: './graph.component.html',
  styleUrl: './graph.component.scss'
})
export class GraphComponent implements AfterViewInit, OnDestroy{
  
  readonly graphStore = inject(GraphStore);
  readonly dataStore = inject(DataStore);

  @ViewChild("graph") graphContainer:ElementRef;

  @Output() nodeSelected = new EventEmitter<GraphNode>();

  filter = input<FilterConfig>()
 

  sidebar = false;
  sidebarItem: string;
  sidebarContent: string;
  sidebarType: EntityType; 

  graph:ForceGraph3DInstance;

  dataSubscription: Subscription;

  constructor(private layoutService:LayoutService){
    var dataObservable = toObservable(this.graphStore.data)
      .pipe(throttleTime(750, null, {leading: true, trailing: true}));
    var filterObservable = toObservable(this.filter);

    this.dataSubscription = combineLatest([dataObservable, filterObservable])
      .pipe(
        tap(x => console.log("tapped", x)),
        map(x => ({nodes:x[0].nodes, edges:x[0].edges, filter: x[1]}))
      )
      .subscribe(x => this.updateData(x.nodes, x.edges, x.filter));
  }
  
  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }



  ngAfterViewInit(): void {
    this.layoutService.closeDesktopMenu()
    this.graph = ForceGraph3D({controlType: "orbit"})(this.graphContainer.nativeElement)
      .linkSource("sourceId")
      .linkTarget("targetId")
      .height(window.innerHeight - 150)
      .width(window.innerWidth - 50)
      .nodeLabel((node:GraphNode) => node.title)
      .nodeColor((node:GraphNode) => this.getNodeColor(node))
      .nodeThreeObjectExtend(true)
      .linkDirectionalArrowLength(3.5)
      .linkColor((link:GraphEdge) => this.getLinkColor(link))
      .linkDirectionalArrowRelPos(1)   
      .linkCurvature((link:GraphEdge) => link.type == "mention" ? 0.2 : 0)
      .linkWidth(((link:GraphEdge) => (this.selectedItem && (link.sourceId == this.selectedItem.id || link.targetId == this.selectedItem.id)) ? 2 : 1))
      .linkDirectionalParticles((link:GraphEdge) => (this.selectedItem && (link.sourceId == this.selectedItem.id || link.targetId == this.selectedItem.id)) ? 4 : 0)
      .linkDirectionalParticleWidth(3)

      .nodeThreeObject((node:GraphNode)=>{
        const obj = new SpriteText(node.title);          
          if(this.selectedItem && this.selectedItem.id === node.id){
            obj.textHeight = 10;
            obj.color = "white";
          }else if (this.selectedItem && this.dependentItemIds.includes(node.id)){
            obj.textHeight = 5;
            obj.color = "#808080"
          }else if (this.selectedItem){
            obj.textHeight = 2;
            obj.color = "black";
          }else{
            obj.textHeight = 5;
            obj.color = "#808080"
          }
          
          obj['position'].add(new Vector3(0, 10,0))
        return obj;
      })
      .onNodeClick((node:GraphNode) => {
        this.selectedItem = node;
        this.refresh();
      });
      
      const linkForce = this.graph.d3Force("link") as any;
      linkForce.strength((link:GraphEdge) => (weightByType[link.type] ?? 25) / (25 / (link.count ?? 1)) * 0.5);

      const centerForce = this.graph.d3Force("center") as any;
      centerForce.strength(0);
  }

  // Visibility Settings
  getNodeColor(node:GraphNode){

    if(this.dependentItemIds.includes(node.id))
      return node.color ?? "white";

    if(this.selectedItem && this.selectedItem.id !== node.id)
      return "#242424";   

    return node.color ?? "white";
  }

  getLinkColor(link:GraphEdge){
    if(this.selectedItem && (this.selectedItem.id === link.sourceId || this.selectedItem.id === link.targetId))
      return this.selectedItem.color ?? "white";

    return link.type == "mention" ? "	#080808" : "white"
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log(event.innerWidth);
    this.graph
      .height(window.innerHeight - 150)
      .width(window.innerWidth - 50);      
  }

  updateData(nodes:GraphNode[], edges:GraphEdge[], filter:FilterConfig){
    console.log("Updating graph", nodes, edges, filter)
    nodes = nodes?.sort((x,y) => x.id.localeCompare(y.id));
    edges = edges?.sort((x,y) => x.id.localeCompare(y.id));
    
    if(filter?.centerNode){
      // We have a center node
      var nodeIds = new Set([filter.centerNode]);
      var filteredEdges = [];
      for(let i = 0; i < (filter.centerNodeDistance ?? 2); i++){
        console.log("Filter pass", nodeIds, filteredEdges);
        filteredEdges = edges.filter(x => nodeIds.has(x.sourceId) && x.type != "mention");
        filteredEdges.forEach(edge => nodeIds.add(edge.targetId));
      }      
    }

    // Find nodes to update, remove and add
    var currentData = this.graph.graphData();
   
    if(currentData.nodes.length == 0){
      currentData.nodes = [...nodes.map(x => ({...x}))];
    }else{
      for(let i = 0; i < nodes.length; i++){
        if(i >= currentData.nodes.length){
          currentData.nodes = [...currentData.nodes, ...nodes.slice(i).map(x => ({...x}))]
          break;
        }
        if(currentData.nodes[i]?.id == nodes[i].id){
          if(currentData.nodes[i]['lastUpdate'] == nodes[i].lastUpdate) continue;
          else currentData.nodes[i] = {...currentData.nodes[i], ...nodes[i]}
        }else{
          var index = nodes.findIndex(x => x.id == currentData.nodes[i].id);
          if(index === -1){
            // Element has been removed
            currentData.nodes.splice(i, 1);
            i--;
          }else{
            currentData.nodes = [...currentData.nodes.slice(0, i), ...nodes.slice(i+1, index + 1).map(x => ({...x})), ...currentData.nodes.slice(i) ]
            i = index;
          }
        }
      }
    }
    if(currentData.links.length == 0){
      console.log("EDGES", edges);
      if(edges){
        currentData.links = [...edges.map(x => ({...x}))];
      }
      
    }else{
      console.log("update");
      for(let i = 0; i < edges.length; i++){
        if(i >= currentData.links.length){
          currentData.links = [...currentData.links, ...edges.slice(i).map(x => ({...x}))]
          break;
        }
        if(currentData.links[i] && currentData.links[i]['id'] == edges[i].id){
          if(currentData.links[i]['lastUpdate'] == edges[i].lastUpdate) continue;
          else currentData.links[i] = {...currentData.links[i], ...edges[i]}
        }else{
          var index = edges.findIndex(x => x.id == currentData.links[i]['id']);
          if(index === -1){
            // Element has been removed
            currentData.links.splice(i, 1);
            i--;
          }else{
            currentData.links = [...currentData.links.slice(0, i), ...edges.slice(i+1, index + 1).map(x => ({...x})), ...currentData.links.slice(i) ]
            i = index;
          }
        }
      }
    }

    console.log(currentData);
    this.graph.graphData({
      nodes: [...currentData.nodes].sort((x,y) => (<string>x.id).localeCompare(<string>y.id)),
      links: [...currentData.links].sort((x,y) => (<string>x['id']).localeCompare(<string>y['id']))
    })
  }

  refresh(){
    this.dependentItemIds = (!this.selectedItem?.id) ? [] : 
      (this.graphStore.outgoingEdgesByNodeId()[this.selectedItem?.id]?.map(x => x.targetId) ?? [])
        .concat((this.graphStore.incomingEdgesByNodeId()[this.selectedItem?.id]?.map(x => x.sourceId) ?? []));
    this.graph.refresh();
  }

  // SEARCH FUNCTIONS
  selectedItem: GraphNode|undefined;
  dependentItemIds: string[] = [];
  suggestions = [];

  search(event:any){
    console.log(event);
    this.suggestions = this.graphStore.nodes().filter(x => x.title.includes(event.query))
  }

}
