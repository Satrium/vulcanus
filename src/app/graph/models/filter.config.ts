export interface FilterConfig{
    displayPrivate?: boolean;
    displayDrafts?: boolean;
    displayWip?: boolean;

    centerNode?: string;
    centerNodeDistance?: number;
}