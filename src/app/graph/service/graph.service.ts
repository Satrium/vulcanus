import { Injectable, inject } from '@angular/core';
import { jLouvain } from 'jlouvain';
import { DataStore } from 'src/app/data/stores/data.store';
import { GraphStore } from './graph.store';
import { toObservable } from '@angular/core/rxjs-interop';
import { filter, throttleTime } from 'rxjs';

export const weightByType = {
  "mention": 1
}

@Injectable({
  providedIn: 'root'
})
export class GraphService {

  readonly graphStore = inject(GraphStore);

  constructor() { 
    console.log("Initiating Graph Store")
    var observable = toObservable(this.graphStore.data)
      .pipe(      
        throttleTime(2500, null, {leading: true, trailing: true}),
      )
      .subscribe(data => {
        if(!data || !data.edges || !data.nodes)
          return;
        console.log("Triggering community calculation", data);
        let community = jLouvain()
          .nodes(data.nodes.map(n => n.id))
          .edges(data.edges.map(n => ({target: n.targetId, source: n.sourceId, weight: weightByType[n.type] ?? 10})))
        let result = community() as {[node:string]:number}
        if(!result)
          return;
        console.log("Calculating communites", result);
        let counts = Object.entries(result).reduce((sums, entry) => {sums[entry[1]] = (sums[entry[1]] || 0) + 1; return sums;}, {});
        var clusters = Object.entries(result).map(x => ({
          nodeId: x[0],
          cluster: counts[x[1]] > 1 ? counts[x[1]] : -1
        })).filter(x => this.graphStore.nodeMap()[x.nodeId].cluster != x.cluster);
        if(clusters.length > 0) this.graphStore.clusterNodes(clusters);
      })
  }
}
