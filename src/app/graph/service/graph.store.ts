import { computed } from "@angular/core";
import { patchState, signalStore, withComputed, withMethods, withState } from "@ngrx/signals";
import distinctColors from 'distinct-colors';

export const colors = distinctColors({count: 75, chromaMin: 20, lightMin: 20})
var colorCounter = 0;

export enum EntityType{
    Article, Category
}

export interface GraphNode {
    id: string;
    type: EntityType
    title: string;
    lastUpdate?: number;
    color?: string;
    cluster?: number;
}

export interface GraphEdge {
    id: string;
    sourceId: string;
    targetId: string;
    type: string;
    lastUpdate?: number;
    count?: number;
}

export interface NodeGroup {
    color: string;
}

export interface GraphState {
    nodeMap: {[id:string]:GraphNode}
    edgeMap: {[id:string]:GraphEdge} 
    nodeGroups: {[id:string]:NodeGroup},
    incomingEdgesByNodeId: {[id:string]:GraphEdge[]},
    outgoingEdgesByNodeId: {[id:string]:GraphEdge[]},
}

const initialState: GraphState = {
    nodeMap: {},
    edgeMap: {},
    nodeGroups: {},
    incomingEdgesByNodeId : {},
    outgoingEdgesByNodeId: {}
}

export const GraphStore = signalStore(
    { providedIn: 'root' },
    withState<GraphState>(initialState),
    withComputed(store => ({
        nodes: computed(() => Object.values(store.nodeMap())),
        edges: computed(() => Object.values(store.edgeMap()).filter(x => x.sourceId in store.nodeMap() && x.targetId in store.nodeMap())),
        data: computed(() => ({
            nodes: Object.values(store.nodeMap()), 
            edges: Object.values(store.edgeMap())
                .filter(x => x.sourceId in store.nodeMap() &&  x.targetId in store.nodeMap())}))
    })),
    withMethods(store => {
        return {
            clear(){
                patchState(store, initialState)
            },
            addNode(node:GraphNode){
                if(!(node.id in store.nodes)){
                    node.lastUpdate = Date.now();
                    patchState(store, (state) => ({
                        nodeMap: {...state.nodeMap, [node.id]: node}
                    }))
                }
            },
            clusterNodes(nodes:{nodeId:string, cluster:number}[]){
                let newgroups: {[key:string]:NodeGroup} = {}
                nodes.forEach(n => {
                    if(n.nodeId in store.nodeMap()){
                        var node = store.nodeMap()[n.nodeId];
                        node.cluster = n.cluster;
                        let color = store.nodeGroups()[n.cluster]?.color ?? newgroups[n.cluster]?.color;
                        if(!color){
                            newgroups[n.cluster] = {color: colors[colorCounter++].hex()}
                            color = newgroups[n.cluster].color;
                        }
                        node.color = color;
                        node.lastUpdate = Date.now();
                    }
                });
                patchState(store, (state) => ({
                    nodeGroups: {...state.nodeGroups, ...newgroups},
                    nodeMap: {...state.nodeMap}
                }));
            },
            addEdge(edge:GraphEdge){
                if(!(edge.id in store.nodes)){
                    edge.lastUpdate = Date.now();
                    patchState(store, (state) => ({
                        edgeMap: {...state.edgeMap, [edge.id]: edge},
                        outgoingEdgesByNodeId: {...state.outgoingEdgesByNodeId, [edge.sourceId]: [...state.outgoingEdgesByNodeId[edge.sourceId] ?? [], edge]},
                        incomingEdgesByNodeId: {...state.incomingEdgesByNodeId, [edge.targetId]: [...state.incomingEdgesByNodeId[edge.targetId] ?? [], edge]}
                    }))
                }
            }
        }
    })
)