import { Component, inject, signal } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/service/user.service';
import { UserStore } from 'src/app/data/stores/user.store';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { environment  } from 'src/environments/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class LoginComponent {

    appVersion = environment.appVersion;

    readonly userStore = inject(UserStore);

    remember: boolean = false;

    loading = signal(false);
    errorMessage = signal("");

    constructor(private router:Router) {}

    async login(){
        this.loading.set(true);
        var success = await this.userStore.login();
        this.loading.set(false);
        if(!success){
            this.errorMessage.set("Invalid World Anvil Token");
        }else{
            this.router.navigate(['/']);
        }
        
    }
}
