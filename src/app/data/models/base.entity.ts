export interface BaseEntity{
    id: string;
    title: string;
    slug: string;
    state: "public|private";
    isWip: boolean;
    isDraft: boolean;
    entityClass: string;
    icon: string;
    url: string;
    folderId: string;
    tags: string;
    updateDate: Date;
}