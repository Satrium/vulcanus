export interface Like{
    entityId: string;
    id: string;
    name: string;
    url: string;
}