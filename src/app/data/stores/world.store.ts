import { inject, computed } from '@angular/core';
import { signalStore, withState, withComputed, withMethods, withHooks, patchState } from '@ngrx/signals';
import { UserDataService, World } from '../service/user.data.service';
import { DataStore } from './data.store';

export interface WorldState{
    worlds: World[];
    activeWorldId: string;
}

const initialState:WorldState = {
    worlds: [],
    activeWorldId: ""
}

export const WorldStore = signalStore(
    { providedIn: 'root' },
    withState<WorldState>(initialState),
    withComputed(store => ({
        activeWorld : computed(() => store.worlds().find(x => x.id == store.activeWorldId()))
    })),
    withMethods(store => {
        var userService = inject(UserDataService);
        var dataStore = inject(DataStore);
        return {
            async load(){
                var worlds = await userService.GetWorlds();
                patchState(store, {worlds:worlds});
                localStorage.setItem("worlds", JSON.stringify(worlds));
            },
            async setActiveWorld(id:string){
                patchState(store, {activeWorldId: id})
                await dataStore.changeWorld(store.activeWorld()!);
                localStorage.setItem("active-world", id);
            },
            async clear(){
                localStorage.removeItem("worlds");
                localStorage.removeItem("active-world");
                patchState(store, initialState);
                await dataStore.clear();
            }
        };
    }),
    withHooks(state => ({
        onInit(){
            var localData = localStorage.getItem("worlds");
            if(!localData) return;
            var worlds = JSON.parse(localData);
            patchState(state, {worlds});

            var activeWorld = localStorage.getItem("active-world");
            if(!activeWorld) return;            
            state.setActiveWorld(activeWorld);
        }
    }))
);