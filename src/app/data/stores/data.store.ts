import { inject, computed } from '@angular/core';
import { signalStore, withState, withComputed, withMethods, withHooks, patchState } from '@ngrx/signals';
import { LoadingStore } from './loading.store';
import { DataDataService, DataEntityInformation } from '../service/data.data.service';

import { DataEntity, db } from '../database';
import { World } from '../service/user.data.service';

import ArticleHelper from '../helpers/article.helper';
import { GraphService } from 'src/app/graph/service/graph.service';
import { EntityType, GraphStore } from 'src/app/graph/service/graph.store';

export interface Article{
    id: string;
    title: string;
    lastUpdate: Date;
    fans: ArticleFan[];
    comments: any[];
    content: string;
    entityClass: string;
    likes?: number;
    views?: number;
}

export interface ArticleFan{
    id: string;
    title: string;
    url: string;
}

export interface DataState{
    worldId: string;
    articles: {[key:string]:Article};
}

const initialState: DataState = {
    worldId: "",
    articles: {}
}

export const DataStore = signalStore(
    { providedIn: 'root' },
    withState<DataState>(initialState),
    withComputed(store => ({
        totalLikes: computed(() => Object.entries(store.articles()).map(x => x[1]?.fans?.length ?? 0).reduce((partialSum, a) => partialSum + a, 0)),
        totalComments: computed(() => Object.entries(store.articles()).map(x => x[1]?.comments?.length ?? 0).reduce((partialSum, a) => partialSum + a, 0)),
        totalArticles:computed(() => Object.entries(store.articles()).length),
        totalWordcount: computed(() => Object.entries(store.articles()).map(x => ArticleHelper.getWordcount(x[1])).reduce((partialSum, a) => partialSum + a, 0)),
        entityState: computed(() => Object.entries(store.articles()).map(x => ({id: x[1].id, "type": 'article', "lastUpdate": x[1].lastUpdate} as DataEntityInformation)))
    })),
    withMethods(store => {
        var loadingState = inject(LoadingStore);
        var dataService = inject(DataDataService);
        var graphStore = inject(GraphStore);
        return {
            async addOrUpdateEntity(entity: DataEntity){
                db.entities.put(entity);
                if(!entity.data){
                    console.warn("Invalid entity tried to save", entity);
                    return;
                }
                switch(entity.type){
                    case 'article':
                        var article : Article = JSON.parse(entity.data);
                        patchState(store, (state) =>({articles:{...state.articles, [entity.id]: {...article, lastUpdate: entity.lastUpdate}}}));
                        graphStore.addNode({id: article.id, title: article.title, type: EntityType.Article});
                        var edges = ArticleHelper.GetLinks(article);
                        edges.forEach(x => graphStore.addEdge(x));
                }
            },
            async changeWorld(world: World){
                patchState(store, {articles: {}})
                graphStore.clear();
                loadingState.start(`Loading world ${world.title}`);
                loadingState.update("Checking local information");
                var entities = await db.entities.where("worldId").equals(world.id).toArray();
                for(const entity of entities){
                    await this.addOrUpdateEntity(entity);
                }  
                loadingState.update("Updating data");
                try{
                    await dataService.RequestUpdate({
                        worldId: world.id,
                        existingEntities: store.entityState(),
                        requestedEntityTypes: ['article', 'category']
                    });
                }catch(e){
                    console.error(e);
                }
               
                console.log("Data updated");
                await loadingState.done();
            },
            async clear(){
                patchState(store, initialState);
                await db.entities.clear();
            }
        };
    })
);