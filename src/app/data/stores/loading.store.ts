import { computed } from '@angular/core';
import { signalStore, withState, withComputed, withMethods, patchState } from '@ngrx/signals';

interface LoadingState{
    isLoading: boolean;
    description: string;
    step: string | undefined;
    value: number | undefined;
    total: number | undefined;
}

const initialState: LoadingState = {
    isLoading: false,
    description: "",
    step: undefined,
    value: undefined,
    total: undefined
}

export const LoadingStore = signalStore(
    { providedIn: 'root' },
    withState<LoadingState>(initialState),
    withComputed(store => ({
        loadingStateProgress: computed(() => 
            ((store.value() ?? 0) / (store?.total() ?? 1)) * 100
        )
    })),
    withMethods(store => ({
        start(description: string): void {
            patchState(store, { isLoading: true, description: description});
        },
        update(step: string, value?: number, total?: number){
            console.log(step, value, total);
            patchState(store, { step, value, total});
        },
        async done(){
            patchState(store, {step: "Finished"})
            await new Promise(r => setTimeout(r, 500)); 
            patchState(store, initialState);
        }
    }))
)   