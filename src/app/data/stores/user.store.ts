import { inject } from '@angular/core';
import { signalStore, withState, withMethods, withHooks, patchState } from '@ngrx/signals';
import { UserDataService } from '../service/user.data.service';
import { WorldStore } from './world.store';

export interface UserState{
    token: string;
    authorized: boolean;
    user: {
        id?: string;
        username?: string;
        profile?: string;
    } | undefined
}


const initialState: UserState =  {
    token: "",
    authorized: true,
    user: undefined
}

export const UserStore = signalStore(
    { providedIn: 'root' },
    withState<UserState>(initialState),
    withMethods(store => {
        var userService = inject(UserDataService);
        var worldStore = inject(WorldStore);
        return {
            async login(){                
                try{
                    var user = await userService.GetUser();
                    patchState(store, {user, authorized: true});
                    localStorage.setItem("user", JSON.stringify({user: store.user(), token: store.token()}));
                    console.log(user);
                    worldStore.load();
                    return true;
                }catch(e) {
                    console.log(e);
                    return false;
                }
            },
            async logout(){
                patchState(store, {token: undefined, authorized: false, user: undefined})
                localStorage.removeItem("user");
                worldStore.clear();
            },
            updateToken(token: string){
                patchState(store, {token})
            }
        }
    }),
    withHooks(state => ({
        onInit(){
            var localData = localStorage.getItem("user");
            if(!localData) return;
            var user = JSON.parse(localData);
            patchState(state, user);
        }
    }))
);