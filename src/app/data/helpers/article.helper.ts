import {Article} from '../stores/data.store';

const ignoredEntityTypes = ["World","User","Image"]

const blockRegex = /\[articleblock:([0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1})\]/gi;
const linkRegex = /\[.*\]\(\S+:([0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1})\)/gi;

export default class ArticleHelper{
    public static getWordcount(article: Article): number{
        return (article?.content?.split(" ") || []).length;
    }

    public static GetLinks(article:Article): {id: string, sourceId: string, targetId: string, type: string}[] {
        var values = Object.entries(article);
        var links = [];
        values.forEach(element => {
            if(element[1] &&typeof element[1] === 'object' && "entityClass" in element[1] 
                && element[1].url
                && !(ignoredEntityTypes.includes(element[1]["entityClass"]))){
                links.push({
                    sourceId: article.id,
                    targetId: element[1]["id"],
                    id: `${article.id}-${element[1]["id"]}-${element[0]}`,
                    type: element[0],
                })
            }else if(element[1] && typeof element[1] === 'string'){
                var blocks = Array.from(element[1].matchAll(blockRegex)).map(x => x[1]);
                links.push(...blocks.map(x => ({sourceId: article.id, targetId: x, id: `${article.id}-${x}-block`, type: 'block'})));

                var mentions = Array.from(element[1].matchAll(linkRegex)).map(x => x[1]);
                links.push(...mentions.map(x => ({sourceId: article.id, targetId: x, id: `${article.id}-${x}-mention`, type: 'mention'})));
            }
        });
        
        const linkMap = new Map(links.map(item => [item['id'], item]));
        
        links.filter(x => x.type == "mention").forEach(element => {
            const el = linkMap.get(element.id);
            el["count"] = (el["cound"] ?? 0) + 1;
        });

        return [...linkMap.values()];
    }

    public static GetContent(article:Article): string{
        var values = Object.entries(article);

        const blackListedFields = [
            "id", "scrapbook", "authornotes", "tags", "url", "icon", "entityClass", "editURL", "displayCss",
            "folderId", "state", "templateType", "customArticleTemplate"
        ];

        var words = "";

        values.forEach(element => {            
            if(element[1] && typeof element[1] === 'string' && !blackListedFields.includes(element[0])){
                var content = element[1].replace("\r\n", " ");

                // Replace mentions
                content = content.replace(/@\[(.*?)]\(.*?\)/mg, "$1")
                    .replace(/\[.*?\]/mg, " ")
                    .replace(/\s\s+/g, ' ');

                

                words += content;
            }
        })
        return words;
    }
}