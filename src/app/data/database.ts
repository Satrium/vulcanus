import Dexie, { Table} from 'dexie';

export interface DataEntity{
    id: string;
    worldId: string;
    type: 'article';
    data: string;
    lastUpdate: Date;
}

export class Database extends Dexie{

    entities : Table<DataEntity>;

    constructor(){
        super("waTools");
        this.version(1).stores({
            entities: 'id, *worldId'
        })
    }
}

export const db = new Database();
console.log(db);
db.open().catch(function (e) {
    console.error("Open failed: " + e.stack);
})