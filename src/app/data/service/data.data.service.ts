import { Inject, Injectable, forwardRef } from '@angular/core';
import { DataEntity } from '../database';
import { firstValueFrom } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { RequestService } from './request.service';

export interface DataEntityInformation{
  id: string;
  type: string;
  lastUpdate: Date;
}

export interface DataEntityRequest{
  worldId: string;
  existingEntities: DataEntityInformation[];
  requestedEntityTypes: ("article"|"category")[]
}

@Injectable({
  providedIn: 'root'
})
export class DataDataService {

  constructor(private http:HttpClient,  private requestService:RequestService) { }
  
  public async RequestUpdate(request: DataEntityRequest): Promise<DataEntity[]>{
    console.log(request);
    var response = this.http.post<DataEntity[]>("/api/world/" + request.worldId, request)
    return await firstValueFrom(response);
  }
}
