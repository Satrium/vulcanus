import { Injectable } from '@angular/core';
import { Observable, takeUntil, firstValueFrom, Subject, mergeMap } from 'rxjs';
import { WebsocketService } from 'src/app/core/service/websocket.service';
@Injectable({
  providedIn: 'root'
})
export class RequestService {
  
  constructor(){}

  public getResponse<T>(observable: Observable<T>) : Promise<T>{
    return firstValueFrom(observable);
  }
}
