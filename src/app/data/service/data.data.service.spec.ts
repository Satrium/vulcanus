import { TestBed } from '@angular/core/testing';

import { DataDataService } from './data.data.service';

describe('DataDataService', () => {
  let service: DataDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
