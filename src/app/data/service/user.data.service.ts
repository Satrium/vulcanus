import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';

export interface User{
  id: string;
  username: string;
  userhash: string;
}

export interface World {
  id: string;
  title: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(private http:HttpClient) { }

  public async GetUser() : Promise<User>{
    var request = this.http.get<User>("/api/user/auth");
    return firstValueFrom(request);
  }

  public async GetWorlds() : Promise<World[]>{
    var request = this.http.get<{success:boolean, entities:World[]}>("/api/user/worlds");
    var result = await firstValueFrom(request);
    return result.entities;
  }
}
