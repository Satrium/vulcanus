import { Component, signal } from '@angular/core';
import { GraphComponent } from '../graph/graph.component';
import { FilterConfig } from '../graph/models/filter.config';
import { GraphNode } from '../graph/service/graph.store';
import { SidebarModule } from 'primeng/sidebar';
import { filter } from 'rxjs';
import { DetailSidebarComponent } from '../shared/detail-sidebar/detail-sidebar.component';

@Component({
  selector: 'app-explore',
  standalone: true,
  imports: [GraphComponent, SidebarModule, DetailSidebarComponent],
  templateUrl: './explore.component.html',
  styleUrl: './explore.component.scss'
})
export class ExploreComponent {
  filterConfig = signal<FilterConfig>({})

  sidebar = false;
  selectedNodeId = null;
  selectedNodeType = null;

  innerFilterConfig = signal<FilterConfig>({})

  nodeSelected(node:GraphNode){
    this.selectedNodeId = node.id;
    this.selectedNodeType = node.type;
    this.sidebar = true;
  }
}
