import { Component, inject } from '@angular/core';
import { LoadingService } from './loading.service';
import { LoadingStore } from 'src/app/data/stores/loading.store';

@Component({
  selector: 'app-loading-information',
  templateUrl: './loading-information.component.html',
  styleUrl: './loading-information.component.scss'
})
export class LoadingInformationComponent {

  readonly loadingState = inject(LoadingStore);
}
