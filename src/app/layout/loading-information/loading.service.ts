import { Injectable, signal, computed } from '@angular/core';
import { Socket } from 'ngx-socket-io';

export interface ProgressUpdate{
  step: string;
  value: number;
  total?: number;
}

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loading = signal<boolean>(false);
  description = signal<string>("");
  step = signal<string>("");
  value = signal<number|undefined>(undefined);
  total = signal<number|undefined>(undefined);
  progress = computed(() => ((this.value() ?? 0) / (this.total() ?? 1)) * 100);

  id = signal<string|undefined>(undefined);

  constructor() { 
    // this.socket.on('connect', () => {
    //   console.log("Socket connection", this.socket.ioSocket.id);
    //   this.id.set(this.socket.ioSocket.id);
    // });
    // this.socket.fromEvent<ProgressUpdate>("progress")
    //   .subscribe(update => {
    //     this.step.set(update.step);
    //     this.value.set(update.value);
    //     this.total.set(update.total);
    //   });
  }

  public start(description:string){
    this.description.set(description);
    this.total.set(undefined);
    this.loading.set(true);
  }

  public async done(){
    this.step.set("Finished");
    await new Promise(r => setTimeout(r, 500));
    this.description.set("");
    this.loading.set(false);
  }
}
