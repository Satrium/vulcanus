import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = [
            {
                label: 'Home',
                items: [
                    { label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: ['/'] },
                    { label: 'Fan Corner', icon: 'pi pi-heart-fill', routerLink: ['fancorner']}
                ]
            },
            {
                label: 'Graph',
                items : [
                    { label: 'Explore', icon: 'pi pi-fw pi-sitemap', routerLink: ['graph']}
                ]
            },    
            {
                label: 'Content',
                items : [
                    {label: 'Articles', icon: 'pi pi-book', routerLink: ['articles']}
                ]
            }       
        ];
    }
}
