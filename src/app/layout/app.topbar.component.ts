import { ChangeDetectionStrategy, Component, ElementRef, Signal, ViewChild, computed, inject, signal } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from "./service/app.layout.service";
import { UserService } from '../core/service/user.service';
import { WorldStore } from '../data/stores/world.store';
import { UserStore } from '../data/stores/user.store';
import { Router } from '@angular/router';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppTopBarComponent {

    readonly worldStore = inject(WorldStore);
    readonly userStore = inject(UserStore);


    items: Signal<MenuItem[]> = computed(() => [
        {
            label: this.userStore.user().username,
            items: [
                {
                    label: "Logout",
                    command: () => {
                        this.userStore.logout().then(x => this.router.navigate(["auth", "login"]));
                    }
                }
            ]
        }
    ]);
    
    selectedWorld: Signal<string> = signal("");

    @ViewChild('menubutton') menuButton!: ElementRef;

    @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

    @ViewChild('topbarmenu') menu!: ElementRef;

    constructor(public layoutService: LayoutService,
        public userService: UserService, public router:Router) { 
        }

    public async onWorldSelect(event:any){
        await this.worldStore.setActiveWorld(event.value);
    }
}
