import { AfterViewInit, Component, Signal, computed, inject, input } from '@angular/core';
import { Article, DataStore } from 'src/app/data/stores/data.store';
import { EntityType, GraphNode, GraphStore } from 'src/app/graph/service/graph.store';

import { CommonModule } from '@angular/common';
import { FieldsetModule } from 'primeng/fieldset';
import { ChipModule } from 'primeng/chip';
import { EdgeTypePipe } from '../../pipes/edge-type.pipe';
import { LinkComponent } from '../../link/link.component';


interface ArticleConnection{
  type: string;
  direction: 'incoming'|'outgoing';
  article: Article;
}

interface NamedConnection{
  type: string;
  direction: 'incoming'|'outgoing';
  node: GraphNode;
}

@Component({
  selector: 'app-article',
  standalone: true,
  imports: [CommonModule, FieldsetModule, LinkComponent],
  templateUrl: './article.component.html',
  styleUrl: './article.component.scss'
})
export class ArticleComponent implements AfterViewInit{
  ngAfterViewInit(): void {
    console.log("Connected Articles", this.connectedArticles())
  }
  
  readonly dataStore = inject(DataStore);
  readonly graphStore = inject(GraphStore);

  articleId = input<string>();
  article = computed(() => this.dataStore.articles()[this.articleId()])
  edges = computed(() => this.graphStore.edges().filter(x => x.sourceId == this.articleId() || x.targetId == this.articleId()))
  connectionNodes:Signal<NamedConnection[]> = computed(
    () => this.edges().map(x => ({
      type: x.type,
      direction: x.targetId == this.articleId() ? 'incoming' : 'outgoing',
      node: this.graphStore.nodeMap()[x.targetId == this.articleId() ? x.sourceId : x.targetId]
    }))
  );
  connectedArticles:Signal<ArticleConnection[]> = computed(
    () => this.connectionNodes()
      .filter(x => x.node.type == EntityType.Article)
      .map(a => ({
        type:a.type,
        direction:a.direction,
        article: this.dataStore.articles()[a.node.id]})
      )
  );
  incomingArticles = computed(() => this.connectedArticles().filter(x => x.direction == 'incoming'));
  outgoingArticles = computed(() => this.connectedArticles().filter(x => x.direction == 'outgoing'))
}
