import { Component, input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article/article.component';

@Component({
  selector: 'app-detail-sidebar',
  standalone: true,
  imports: [CommonModule, ArticleComponent],
  templateUrl: './detail-sidebar.component.html',
  styleUrl: './detail-sidebar.component.scss'
})
export class DetailSidebarComponent {
  entityType = input<string>()
  entityId = input<string>()
}
