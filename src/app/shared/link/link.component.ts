import { AfterViewInit, Component, computed, input } from '@angular/core';
import { ChipModule } from 'primeng/chip';

@Component({
  selector: 'app-link',
  standalone: true,
  imports: [ChipModule],
  templateUrl: './link.component.html',
  styleUrl: './link.component.scss'
})
export class LinkComponent{

  edgeType = input<string>();
  entityType = input<string>();
  entityId = input<string>();
  entityLabel = input<string>();
  direction = input<"outgoing"|"incoming">();

  label = computed(() => this.getLabel(this.edgeType(), this.direction()));
  backgroundColor = computed(() => this.getBackgroundColor(this.edgeType()));
  color = computed(() => this.getColor(this.edgeType()));

  private getLabel(edgeType:string, direction:'outgoing'|'incoming'): string{    
    switch(direction){
      case 'outgoing':
        return this.getOutgoingLabel(edgeType);
      case 'incoming':
        return this.getIncomingLabel(edgeType);
      default:
        return edgeType;
    }
  }

  private getIncomingLabel(edgeType:string){
    switch(edgeType){
      case "parentBiological1":
      case "parentBiological2":
      case "parentBiological2":
        return "Child";
      case "familyorganization":
        return "Members";
      case "primarygeographicLocation":
      case "secondarygeographicLocation":
        return "Appeared In";
      case "parent":
      case "articleParent": 
        return "Contains";
      case "currentLocation":
        return "Resides";
      case "mention":
      case "block":
        return "Mentioned In";
      case "organization":
        return "Includes";
      case "species":
        return "Member of Species"
      case "ethnicity":
        return "Member of Ethnicity"
      default:
        return edgeType;
    }
  }

  private getOutgoingLabel(edgeType:string){
    switch(edgeType){
      case "mention":
      case "block":
        return "Mentions";
      case "parentBiological1":
      case "parentBiological2":
      case "parentBiological2":
        return "Parent";
      case "familyorganization":
        return "Family";
      case "parent":
      case "organization":
        return "Part Of";
      case "articleParent":
        return "Under";
      case "ethnicity":
        return "Ethnicity";
      case "primarygeographicLocation":
      case "secondarygeographicLocation":
        return "Happened in"

      case "species":
        return "Species";
      default:
        return edgeType;
    }
  }

  private getBackgroundColor(edgeType: string): string{
    switch(edgeType){
      case "mention":
        return "grey";     
      case "block":
        return "#660053"; 
      case "articleParent":
        return "#b37502";
      default:
        return "#6366f1";
    }
  }

  private getColor(edgeType: string): string{
    switch(edgeType){
      default:
        return "white";
    }
  }
}
