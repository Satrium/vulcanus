import { AfterViewInit, Component, computed, inject, input } from '@angular/core';
import { GraphStore } from 'src/app/graph/service/graph.store';
import { LinkComponent } from '../link/link.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-detail-page',
  standalone: true,
  imports: [CommonModule, LinkComponent],
  templateUrl: './detail-page.component.html',
  styleUrl: './detail-page.component.scss'
})
export class DetailPageComponent{
  
  readonly graphStore = inject(GraphStore);

  type = input<string>()
  id = input<string>()

  incomingConnections = computed(() => this.graphStore.edges().filter(x => x.sourceId == this.id())
    .map(x => ({type: x.type, node: this.graphStore.nodeMap()[x.targetId]}))
  );
  outgoingConnections = computed(() => this.graphStore.edges().filter(x => x.targetId == this.id())
  .map(x => ({type: x.type, node: this.graphStore.nodeMap()[x.sourceId]}))
  )

}
