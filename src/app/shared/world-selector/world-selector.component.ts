import { Component, computed, inject, signal } from '@angular/core';
import { WorldStore } from 'src/app/data/stores/world.store';

@Component({
  selector: 'app-world-selector',
  templateUrl: './world-selector.component.html',
  styleUrl: './world-selector.component.scss'
})
export class WorldSelectorComponent {
  readonly worldStore = inject(WorldStore);
  
  filterValue = "";
  filter = signal<string>(this.filterValue);
  filteredOptions = computed(() => 
    this.worldStore.worlds().filter(x => x.title.startsWith(this.filter()))
  )
}
