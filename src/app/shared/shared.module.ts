import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WorldSelectorComponent } from './world-selector/world-selector.component';

import { DropdownModule } from 'primeng/dropdown';
import { InputGroupModule } from 'primeng/inputgroup';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    WorldSelectorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DropdownModule,
    InputGroupModule,    
    ButtonModule,
  ], exports: [
    WorldSelectorComponent
  ]
})
export class SharedModule { }
