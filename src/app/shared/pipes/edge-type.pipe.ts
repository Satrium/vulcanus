import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'edgeType',
  standalone: true
})
export class EdgeTypePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    switch(value){
      case "parentBiological1":
      case "parentBiological2":
      case "parentBiological2":
        return "parent";
      case "familyorganization":
        return "family";
      default:
        return value;
    };
  }

}
