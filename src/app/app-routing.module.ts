import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppLayoutComponent } from "./layout/app.layout.component";
import { AuthGuard } from './core/guard/auth.guard';
import { DetailPageComponent } from './shared/detail-page/detail-page.component';
import { NotfoundComponent } from './layout/notfound/notfound.component';
import { FanCornerComponent } from './modules/fan-corner/fan-corner.component';
import { ArticleListComponent } from './modules/article-list/article-list.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', component: AppLayoutComponent,
                canActivate: [AuthGuard],
                children: [
                    { path: '', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
                    { path: 'graph', loadChildren: () => import('./explore/explore.module').then(m => m.ExploreModule) },
                    { path: 'details/:type/:id', component: DetailPageComponent },    
                    { path: 'fancorner', component: FanCornerComponent },
                    { path: 'articles', component: ArticleListComponent}
                ]
            },
            { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
            { path: 'notfound', component: NotfoundComponent },
            { path: '**', redirectTo: '/notfound' },
        ], { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload', bindToComponentInputs: true })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
