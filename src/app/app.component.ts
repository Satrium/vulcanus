import { Component, HostListener, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { RequestService } from './data/service/request.service';
import { WebsocketService } from './core/service/websocket.service';
import { GraphService } from './graph/service/graph.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(private primengConfig: PrimeNGConfig, private websocket:WebsocketService, private graph:GraphService) { }

    ngOnInit() {
        this.primengConfig.ripple = true;
    }
}
