import { Injectable, inject } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserStore } from '../../data/stores/user.store';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    readonly userStore = inject(UserStore)

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log(this.userStore.token())
    if(this.userStore.token()){
      request = request.clone({
        setHeaders: {
          "x-auth-token":this.userStore.token()
        }
      });
    }    
    return next.handle(request);
  }
}
