import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, filter, mergeMap } from 'rxjs';
import { WebsocketService } from 'src/app/core/service/websocket.service';


@Injectable()
export class LoadingIdInterceptor implements HttpInterceptor {

  constructor(private websocketService:WebsocketService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.websocketService.id$.pipe(
      filter(x => !!x),
      mergeMap(id => {
        request = request.clone({
          setHeaders: {
            "X-Loading-Id" : id
          }
        });
        return next.handle(request);
      })
    );
  }
}