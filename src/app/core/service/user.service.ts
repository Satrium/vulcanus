import { HttpClient } from '@angular/common/http';
import { Injectable, Signal, WritableSignal, computed, inject, signal } from '@angular/core';
import { LoadingStore } from 'src/app/data/stores/loading.store';
import { LoadingService } from 'src/app/layout/loading-information/loading.service';
import { LayoutService } from 'src/app/layout/service/app.layout.service';


export interface User{
  id: string;
  username: string;
  userhash: string;
}

interface EntityList<T>{
  success: true;
  entities: T[];
}

interface World{
  id: string;
  title: string;
  state?: string;
  url?: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly loading = inject(LoadingStore);

  public UserToken:WritableSignal<string> = signal("");
  public User:WritableSignal<User|undefined> = signal(undefined);

  public Worlds:WritableSignal<World[]> = signal([
    {
      id: "",
      title: "Lothrea"
    },{
      id: "",
      title: "Test"
    }
  ]);
  public WorldNames:Signal<string[]> = computed(() => this.Worlds().map(x => x.title));
  public SelectedWorld:WritableSignal<World|undefined> = signal(undefined);

  constructor(public layout: LayoutService,
    public http:HttpClient) { }

  async selectWorld(worldName:string){
    var world = this.Worlds().find(x => x.title === worldName);
    console.log(worldName, world);
    this.SelectedWorld.set(world);
    
    this.loading.start("Loading information about your world " + worldName);
    await this.http.get<string>('http://localhost:4201/xyz').toPromise();
    this.loading.done();
    localStorage.setItem("activeWorld", worldName);    
  }

  public async login(userToken:string): Promise<boolean>{
    this.UserToken.set(userToken);
    var response = await this.http.get<User>("http://localhost:4201/user/auth").toPromise();
    if(response){
      this.User.set(response);
      var worlds = await this.http.get<EntityList<World>>("http://localhost:4201/user/worlds").toPromise();
      console.log(worlds);
      if(!worlds || !worlds.success){
        return false;
      }
      this.Worlds.set(worlds.entities);
      return true;
    }
    return false;
  }
}
