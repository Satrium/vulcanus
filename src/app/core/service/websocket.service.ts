import { Injectable, inject, signal } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { LoadingStore } from '../../data/stores/loading.store';
import { DataStore } from 'src/app/data/stores/data.store';
import { DataEntity } from 'src/app/data/database';
import { BehaviorSubject, Observable, ReplaySubject, filter, map, pipe, sampleTime, tap } from 'rxjs';
import { toObservable } from '@angular/core/rxjs-interop';

export interface ProgressUpdate{
  step: string;
  value: number;
  total?: number;
}

@Injectable()
export class WebsocketService {

  readonly loading = inject(LoadingStore);
  readonly data = inject(DataStore);

  id = signal<string>("");
  id$ = toObservable(this.id);

  constructor(private socket:Socket) { 
    this.socket.on('connect', () => {
      console.log("Connected", this.socket.ioSocket.id);
      this.id.set(this.socket.ioSocket.id);
    })
    
    this.id$.subscribe(x => console.log("Socket connection published", x));


    

    this.socket.fromEvent<ProgressUpdate>("progress")
      .subscribe(update => {
        this.loading.update(update.step, update.value, update.total);
      });

    this.socket.fromEvent<DataEntity>("data-entity")
      .subscribe(dataEntity => {
        this.data.addOrUpdateEntity(dataEntity);
      })
  }  
}
