import { Injectable, inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';
import { UserStore } from 'src/app/data/stores/user.store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    readonly userStore = inject(UserStore);

  constructor(private userService: UserService, private router: Router) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.userStore.token()) {
        // redirect to some view explaining what happened
        this.router.navigateByUrl('/auth/login');
        return false;
      } else {
        return true;
      }
  }
  
}
