import { NgModule } from '@angular/core';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppLayoutModule } from './layout/app.layout.module';
import { SharedModule } from './shared/shared.module';

import { CoreModule } from './core/core.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { LoadingIdInterceptor } from './core/interceptor/loading-id.interceptor';
import { AuthInterceptor } from './core/interceptor/auth.interceptor';
import { WebsocketService } from './core/service/websocket.service';
import { GraphService } from './graph/service/graph.service';
import { EdgeTypePipe } from './shared/pipes/edge-type.pipe';

const config: SocketIoConfig = { url: '/', options: {
  path: '/api/socket.io'
} };

@NgModule({
    declarations: [AppComponent],
    imports: [AppRoutingModule, AppLayoutModule, SocketIoModule.forRoot(config), CoreModule, SharedModule],
    providers: [
        { provide: LocationStrategy, useClass: PathLocationStrategy },      
        WebsocketService,
        GraphService,
        EdgeTypePipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LoadingIdInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
