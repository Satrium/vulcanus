import { CommonModule } from '@angular/common';
import { Component, computed, inject } from '@angular/core';
import { DataStore } from 'src/app/data/stores/data.store';

import { TableModule } from 'primeng/table';
import { DashboardModule } from 'src/app/dashboard/dashboard.module';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'app-fan-corner',
  standalone: true,
  imports: [CommonModule, TableModule, DashboardModule, ButtonModule, InputTextModule],
  templateUrl: './fan-corner.component.html',
  styleUrl: './fan-corner.component.scss'
})
export class FanCornerComponent {
  readonly dataStore = inject(DataStore);

  likes = computed(() => {
    var articles = Object.values(this.dataStore.articles());
    var likes = {};
    articles?.forEach(article => {      
      article.fans?.forEach(fan => {
        if(!likes[fan.id])
          likes[fan.id] = {id:fan.id, name:fan.title, author_profile:fan.url, likes: 0, comments: 0, articles: []}
        likes[fan.id].likes++;
        likes[fan.id].articles.push(article.id);
      });
      article.comments?.forEach(comment => {
        if(likes[comment.id]) likes[comment.id].comments++;
      })
    });
    return Object.values(likes);
  })
}
