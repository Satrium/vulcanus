import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FanCornerComponent } from './fan-corner.component';

describe('FanCornerComponent', () => {
  let component: FanCornerComponent;
  let fixture: ComponentFixture<FanCornerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FanCornerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FanCornerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
