import { Component, computed, inject } from '@angular/core';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { DataStore } from 'src/app/data/stores/data.store';
import { GraphStore } from 'src/app/graph/service/graph.store';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';


@Component({
  selector: 'app-article-list',
  standalone: true,
  imports: [TableModule, TagModule, TooltipModule, InputTextModule],
  templateUrl: './article-list.component.html',
  styleUrl: './article-list.component.scss'
})
export class ArticleListComponent {
    readonly dataStore = inject(DataStore);
    readonly graphStore = inject(GraphStore);
    articleList = computed(() => {
      return Object.values(this.dataStore.articles()).map(x => ({
        ...x, 
        comments: x.comments?.length ?? 0,
        likes: x.likes ?? 0,
        views: x.views ?? 0,
        incoming: this.graphStore.incomingEdgesByNodeId()[x.id]?.length ?? 0,
        outgoing: this.graphStore.outgoingEdgesByNodeId()[x.id]?.length ?? 0,
      }));
    });
}
