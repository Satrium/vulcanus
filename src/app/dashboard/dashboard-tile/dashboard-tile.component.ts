import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-tile',
  templateUrl: './dashboard-tile.component.html',
  styleUrl: './dashboard-tile.component.scss'
})
export class DashboardTileComponent {
  @Input() name = "";
  @Input() value = 0;
  @Input() icon = "";

  @Input() iconColor = "white";
  @Input() iconBackground = "";
}
