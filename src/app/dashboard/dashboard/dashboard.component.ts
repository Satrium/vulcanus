import { Component, computed, inject } from '@angular/core';
import ArticleHelper from 'src/app/data/helpers/article.helper';
import { DataStore } from 'src/app/data/stores/data.store';
import { WorldStore } from 'src/app/data/stores/world.store';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  readonly worldStore = inject(WorldStore);
  readonly dataStore = inject(DataStore);

  readonly wordCount = computed(() => {
    let content = Object.values(this.dataStore.articles()).reduce((words, article) => words + " " + ArticleHelper.GetContent(article), "");
    let words = content.split(/\s/);
    return words.length;
  })
}
