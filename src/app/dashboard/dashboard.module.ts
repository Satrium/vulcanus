import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarkdownModule } from 'ngx-markdown';
import { CardModule } from 'primeng/card';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardTileComponent } from './dashboard-tile/dashboard-tile.component';
import { ButtonModule } from 'primeng/button';
import { GraphComponent } from '../graph/graph.component';


@NgModule({
  declarations: [
    DashboardComponent,
    DashboardTileComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CardModule,
    ButtonModule,
    GraphComponent,
    MarkdownModule.forRoot()
  ],  exports:[
    DashboardTileComponent
  ]
})
export class DashboardModule { }
