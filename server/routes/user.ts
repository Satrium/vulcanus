import { Router, Request } from "express";
import { get } from "../worldanvil/request";

export const userRouter = Router();

interface Identity{
    id: string;
    username: string;
    userhash: string;
}

interface UserInfo{
    id: string;
    title: string;
    avatar:{
        id: number;
        url: string;
    }
}

interface EntityList<T>{
    sucess: true;
    entities: T;
}

interface World{
    id: string;
    title: string;
    state: string;
    url: string;
}

export type CustomRequest = Request & {userToken: string; user: Identity;}

export const requireAuth = async (req:CustomRequest, res:any, next:any) => {
    console.log(req.header("X-Auth-Token"));
    if(!req.header("X-Auth-Token")){
        res.status(401);
        return next();
    }

    // TODO: Caching    
    var identity = await get<Identity>("identity", req.header("X-Auth-Token")!);
    if(!identity){
        res.status(401);
        return next();
    }

    req.userToken = req.header("X-Auth-Token")!;
    req.user = identity!;
    return next();
}

userRouter.get('/auth', requireAuth, async (req:CustomRequest, res:any) => {
    var additionalUserInfo = await get<UserInfo>(`user?id=${req.user.id}`, req.header("X-Auth-Token")!);
    res.json({...req.user, profile: additionalUserInfo?.avatar?.url});
})

userRouter.get('/worlds', requireAuth, async (req:CustomRequest, res:any) => {
    var worlds = await get<EntityList<World>>("user/worlds?id=" + req.user.id, req.userToken, "post");
    res.json(worlds);
});