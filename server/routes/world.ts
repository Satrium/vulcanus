import { Router, Request } from "express";
import { get } from "../worldanvil/request";
import { requireAuth, CustomRequest } from './user';
import { Connection } from '../socket';

export const worldRouter = Router();

export interface DataEntityInformation{
    id: string;
    type: string;
    lastUpdate: Date;
}

export interface DataEntityRequest{
    worldId: string;
    existingEntities: DataEntityInformation[];
    requestedEntityTypes: ("article"|"category")[]
}
export interface DataEntity{
    id: string;
    worldId: string;
    type: 'article';
    data: string;
    lastUpdate: Date;
}

interface BaseEntity{
    id: string;
    updateDate: {
        date: Date;
    }
}

interface EntityList{
    sucess: true;
    entities: BaseEntity[];
}


worldRouter.post('/:worldId', requireAuth, async (req:CustomRequest, res:any,) => {
    let request = req.body as DataEntityRequest;
    let existingEntities:{[key:string]:DataEntityInformation} = {}

    let canceled = {value: false};
    req.on('close', () => {
        console.log("Request has been canceled");
        canceled.value = true;
    });

    req.socket.on('close', () => {
        console.log("Request has been canceled");
        canceled.value = true;
    })

    // Create dictionary of existing entities because it's easier to handle
    for(const e of request.existingEntities){
        existingEntities[e.id] = e;
    }

    var connectionId = req.header("X-Loading-Id") as string;
    var entities: DataEntity[] = [];

    Connection.sendProgress(connectionId, {step:"Loading World Information", value:0});
    if(request.requestedEntityTypes.includes('article')){
        var articles = await loadArticles(request.worldId, existingEntities, connectionId, req.userToken, canceled);
        entities.push(...articles);
    }    
    res.json(entities);
});


async function loadArticles(worldId:string, existingEntities:{[key:string]:DataEntityInformation}, connectionId: string, userToken: string, canceled: {value: boolean}): Promise<DataEntity[]>{
    Connection.sendProgress(connectionId, {step:"Loading Articles from your world", value:0});

    var articles: BaseEntity[] = [];

    do{
        var request = await get<EntityList>(`world/articles?id=${worldId}`, userToken, "post", {
            "offset": `${articles.length}`,
            "limit": "50"
        });
        articles.push(...request!.entities);
    }while(request!.entities.length >= 50 && !canceled.value);

    var articles_to_update = articles.filter(x => !(x.id in existingEntities) || existingEntities[x.id].lastUpdate < x.updateDate.date);

    var loaded_articles:DataEntity[] = [];

    var articles_to_load_count = articles_to_update?.length ?? 0;

    for(let i = 0; i < articles_to_load_count && !canceled.value; i++){
        const article = articles_to_update![i];
        Connection.sendProgress(connectionId, {step: "Updating articles", value: i + 1, total:articles_to_update?.length});
        var article_request = await get(`article?id=${article.id}&granularity=3`, userToken);
        var entity:DataEntity = {
            id: article.id,
            lastUpdate: article.updateDate.date,
            type: 'article',
            worldId: worldId,
            data: JSON.stringify(article_request),
        };
        Connection.sendEntity(connectionId, entity);
        loaded_articles.push(entity);
    }   
    return loaded_articles;
}