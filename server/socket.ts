
import socketIO, { Socket } from 'socket.io';
import {DataEntity} from './routes/world';

export interface ProgressUpdate{
    step: string;
    value: number;
    total?: number;
  }

export class Connection{
    public id:string;
    public socket:Socket;

    private constructor(socket:Socket){
        this.id = socket.id;
        this.socket = socket;
    }

    private static connectionList: {[key:string]:Connection} = {};

    public static addConnection(socket:Socket){
        console.log("Client connected", socket.id);
        var connection = new Connection(socket);
        this.connectionList[connection.id] = connection;
        socket.on('disconnect', () => this.removeConnection(socket));
    }

    public static removeConnection(socket:Socket){
        if(socket.id in this.connectionList){
            delete this.connectionList[socket.id];
        }
    }

    public static sendProgress(id:string, update:ProgressUpdate){
        if(id in this.connectionList){
            this.connectionList[id].socket.emit('progress', update);
        }
    }

    public static sendEntity(id:string, entity: DataEntity){
        if(id in this.connectionList){
            this.connectionList[id].socket.emit('data-entity', entity);
        }
    }
}