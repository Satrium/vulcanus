import express from 'express';
import { Request } from 'express';
import { Server, Socket } from 'socket.io';
import http from 'http';
import {Connection} from './socket';

import { userRouter } from './routes/user';
import { worldRouter } from './routes/world';

const app = express();
const httpServer = new http.Server(app);
// Allow any method from any host and log requests
app.use((req:any, res:any, next:any) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, X-Loading-Id, x-auth-token');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
    if('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        console.log(`${req.ip} ${req.method} ${req.url}`);
        next();
    }
});

const io = new Server(httpServer, { cors: { origin: '*' } });

io.on('connection', (client: Socket) => {
    Connection.addConnection(client);
});

// Handle POST requests that come in formatted as JSON
app.use(express.json())

app.use("/user", userRouter);
app.use("/world", worldRouter);

app.get('/:connection', async (req:Request, res:any) => {
    var connectionId = req.header("X-Loading-Id") as string;
    console.log("Connection", connectionId);
    Connection.sendProgress(connectionId, {step:"Loading World Information", value:0});
    await new Promise(r => setTimeout(r, 1000));
    const totalCount = 10;
    Connection.sendProgress(connectionId, {step:"Loading Articles", value:0, total: totalCount});
    for(let i = 1; i <= totalCount; i++){
        Connection.sendProgress(connectionId, {step:"Loading Articles", value: i, total: totalCount});
        await new Promise(r => setTimeout(r, 250));
    }
    res.send({hello: 'world'});
});

// A default hello word route
app.get('/', (req:any, res:any) => {
    res.send({hello: 'world'});
});


// start our server on port 4201
httpServer.listen(4201, '0.0.0.0', function() {
    console.log("Server now listening on 4201");
});