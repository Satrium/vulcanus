import fetch from "node-fetch";
import * as packageinfo from '../package.json';
import Bottleneck from 'bottleneck';

const baseUrl = "https://www.worldanvil.com/api/external/boromir/";

const limiter = new Bottleneck({
    id: "vulcanus",
    reservoir: 25,
    reservoirIncreaseInterval: 250,
    reservoirIncreaseAmount: 10,
    maxConcurrent: 10,
    minTime: 100,
})

export async function get<T>(url:string, userToken:string, method:string = "get", body:any = undefined) : Promise<T|undefined>{
    let headers = {
        'x-application-key': process.env.APP_KEY as string,
        'x-auth-token': userToken,
        'Content-Type': "application/json",
        'User-Agent': `${packageinfo.name} ("linksoftheanvil", ${packageinfo.version})`
    }
    
    let response = await limiter.schedule(async () => {
        return await request<T>(baseUrl + url, headers, method, body ? JSON.stringify(body): undefined);
    });
    return response;
}

const retryCodes = [408, 500, 502, 503, 504, 522, 524];
async function request<T>(url:string, headers:any, method:string, body:any, retries:number = 3) : Promise<T>{
    console.log("Requesting ", url, body);
    let res = await fetch(url, {headers, method, body, signal: AbortSignal.timeout(2500)});
    if(res.ok) return res.json();

    console.log(res.status);

    if(retries > 0 && retryCodes.includes(res.status))
        return request(url, headers, method, body, retries - 1)
    else
        throw new Error(res.statusText);
}