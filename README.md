# Vulcanus


# Development Setup
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.4.
It's using the [PrimeNG](https://primeng.org/) component library and is based on the [Sakai](https://www.primefaces.org/sakai-ng/#/) template

To run Vulcanus you need a WorldAnvil Application Key. Those keys are available to Grandmaster+ subscribers and are nessesary to access the WorldAnvil API
If you have a key, you can provide it as an environment variable `APP_KEY=<your-wa-appliation-key>`


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

