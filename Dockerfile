FROM nginx:1.24.0

COPY ./dist/vulcanus /usr/share/nginx/html
COPY ./deploy/nginx.conf /etc/nginx/conf.d/default.conf